mport argparse
import threading
'''
argparse analiza los argumentos de la línea de comando
threading permite el uso de hebras
'''

def remove_defline_and_newlines(input_file, output_file):
    with open(input_file, 'r') as f_input, open(output_file, 'w') as f_output:
        for line in f_input:
            line = line.strip() #Elimina espacios en blanco
            if not line.startswith('>'): #Se salta los defline
                f_output.write(line)

def process_fasta_file(input_file):
    output_file = input_file + '_processed.fasta' #Se cambia el nombre del archivo ingresado diferenciandolo del de salida
    remove_defline_and_newlines(input_file, output_file)
    print(f'Archivo procesado: {output_file}')

if __name__ == '__main__':
    #Se crea un objeto Argumentparser
    parser = argparse.ArgumentParser(description='Procesar archivos FASTA')
    '''
    Se le asigna nombre files a uno o más archivos FASTA
    metavar='file' permite que se muestre file como el nombre del/los argumentos, en
    los mensajes de ayuda, en caso que no se haya ingresado ningún argumento
    nargs='+' señala que se esperan uno o más valores en los argumentos
    '''
    parser.add_argument('files', metavar='file', nargs='+')
    #Revisa los argumentos entregados por línea de comando
    args = parser.parse_args()

    #Se crea una lista para las hebras
    threads = []
    for fasta_file in args.files:
        #Crea un objeto Thread
        t = threading.Thread(target=process_fasta_file, args=(fasta_file,))
        #Agrega los objetos Thread a la lista threads
        threads.append(t)
        #Inicia la ejecución del hilo
        t.start()

    for t in threads:
        #Espera la señal de que el hilo completó el trabajo
        t.join()

    print('Procesamiento completo')